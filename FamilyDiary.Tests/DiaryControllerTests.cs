﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FamilyDiary.Controllers;
using System.Web.Mvc;
using FamilyDiary.Models;
using Moq;
using Postal;
using System.Net.Mail;
using System.Web;
using FamilyDiary.RequestModels;

namespace FamilyDiary.Tests
{
    [TestClass]
    public class DiaryControllerTests
    {
        private Mock<IRepository> _repo;
        private Mock<IFormsAuthenticationService> _formsAuth;
        private Mock<IHtmlSanitizer> _sanitizer;
        private Mock<IEmailService> _emailService;
        private Mock<IMembershipService> _membership;
        private Mock<IFileSystem> _fileSystem;
        private Mock<HttpServerUtilityBase> _httpServerUtility;

        private DiaryController GetDiaryController()
        {
            _formsAuth = new Mock<IFormsAuthenticationService>();

            _repo = new Mock<IRepository>();
            var questions = new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1}
            };
            _repo.Setup(r => r.GetAllQuestions()).Returns(questions);
            _repo.Setup(r => r.GetAllAnswers()).Returns(new List<Answer>());

            _sanitizer = new Mock<IHtmlSanitizer>();
            _sanitizer.Setup(s => s.Sanitize(It.IsAny<string>())).Returns((string s) => s);
            
            _emailService = new Mock<IEmailService>();

            _membership = new Mock<IMembershipService>();
            _membership.Setup(m => m.GetAllUserEmailAddress()).Returns(new string[1] { "test@farmas.net" });

            _httpServerUtility = new Mock<HttpServerUtilityBase>();
            _httpServerUtility.Setup(s => s.MapPath("~/Views/Emails/NewAnswerEmail.htm"))
                .Returns("serverpath_answer");
            _httpServerUtility.Setup(s => s.MapPath("~/Views/Emails/NewQuestionEmail.htm"))
                .Returns("serverpath_question");

            var httpContext = new Mock<HttpContextBase>();
            httpContext.SetupGet(c => c.Server).Returns(_httpServerUtility.Object);

            _fileSystem = new Mock<IFileSystem>();
            _fileSystem.Setup(f => f.FileReadAllText("serverpath_answer"))
                .Returns("{QUESTION}|{USERNAME}|{ANSWER}|{QUESTIONID}");
            _fileSystem.Setup(f => f.FileReadAllText("serverpath_question"))
                .Returns("{QUESTION}|{USERNAME}|{QUESTIONID}");

            var controller = new DiaryController();
            controller.Repository = _repo.Object;
            controller.FormsService = _formsAuth.Object;
            controller.HtmlSanitizer = _sanitizer.Object;
            controller.EmailService = _emailService.Object;
            controller.MembershipService = _membership.Object;
            controller.FileSystem = _fileSystem.Object;
            controller.ControllerContext = new ControllerContext() { HttpContext = httpContext.Object };

            return controller;
        }

        [TestMethod]
        public void WhenPostingNewAnswer_IfCheckboxNotSet_ShouldNotSendEmail()
        {
            //arrange
            var controller = GetDiaryController();
            _emailService.Setup(e => e.Send(It.IsAny<MailMessage>())).Throws(new Exception("Did not expect email to be sent"));
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");            

            var model = new AnswerRequestModel()
            {
                QuestionId = 1,
                Text = "Respuesta",
                UserName = "foo",
                SendMail = false
            };

            //act
            controller.AddAnswer(model);
        }

        [TestMethod]
        public void WhenPostingNewAnswer_IfCheckboxIsChecked_ShouldSendEmailToAllUsers()
        {
            //arrange
            var controller = GetDiaryController();
            _membership.Setup(m => m.GetAllUserEmailAddress()).Returns(new string[1] { "test@farmas.net" });
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");

            var model = new AnswerRequestModel()
            {
                QuestionId = 1,
                Text = "Respuesta",
                UserName = "foo",
                SendMail = true
            };

            //act
            controller.AddAnswer(model);

            //assert
            _emailService.Verify(s => s.Send(It.Is<MailMessage>(e =>
                e.Subject == "Nueva respuesta de farmas!"
                && e.IsBodyHtml
                && e.From.Address == "fede@farmas.net"
                && e.To.Count == 1
                && e.To[0].Address == "test@farmas.net"
                && e.Body == "Long1|farmas|Respuesta|1")));
        }

        [TestMethod]
        public void WhenPostingNewQuestion_IfMailCheckboxNotSet_ShouldNotSendMail()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            _repo.Setup(r => r.InsertQuestion(It.IsAny<Question>())).Returns(123);
            _emailService.Setup(e => e.Send(It.IsAny<MailMessage>())).Throws(new Exception("Did not expect to send email"));

            var model = new QuestionRequestModel()
            {
                Short = "short",
                Long = "long",
                SendMail = false
            };

            //act
            controller.AddQuestion(model);
        }
       
        [TestMethod]
        public void WhenPostingNewQuestion_IfMailCheckboxSet_ShouldSendEmailToAllUsers()
        {
            //arrange
            var controller = GetDiaryController();
            _membership.Setup(m => m.GetAllUserEmailAddress()).Returns(new string[2] { "fede@farmas.net", "clots@farmas.net" });
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            _repo.Setup(r => r.InsertQuestion(It.IsAny<Question>())).Returns(123);
            var model = new QuestionRequestModel()
            {
                Short = "Short",
                Long = "Long",
                SendMail = true
            };

            //act
            controller.AddQuestion(model);

            //assert
            _emailService.Verify(s => s.Send(It.Is<MailMessage>(e =>
                e.Subject == "Nueva pregunta de farmas!"
                && e.IsBodyHtml
                && e.From.Address == "fede@farmas.net"
                && e.Body == "Long|farmas|123"
                && e.To.Count == 2
                && e.To[0].Address == "fede@farmas.net"
                && e.To[1].Address == "clots@farmas.net")));
             
        }

        [TestMethod]
        public void WhenPostingNewAnswer_ShouldRedirectToMainWithCurrentQuestionId()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            var model = new AnswerRequestModel()
            {
                QuestionId = 1,
                Text = "Answer Text",
                UserName = "farmas"
            };

            //act
            var result = controller.AddAnswer(model) as JsonResult;

            //assert
            Assert.AreEqual("Answer Text", result.Data.ToString());
        }

      

        [TestMethod]
        public void WhenPostingNewAnswer_ShouldCallSanitizerToEncodeText()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            var model = new AnswerRequestModel()
            {
                QuestionId = 1,
                UserName = "farmas",
                Text = "foo" };

            //act
            controller.AddAnswer(model);

            //assert
            _sanitizer.Verify(s => s.Sanitize("foo"));
        }

        [TestMethod]
        public void WhenPostigNewAnswer_ShouldAddAnswerToRepo()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            _sanitizer.Setup(s => s.Sanitize("Answer Text")).Returns("Answer Text");

            var model = new AnswerRequestModel()
            {
                QuestionId = 1,
                UserName = "farmas",
                Text = "Answer Text"
            };

            //act
            controller.AddAnswer(model);

            //assert
            _repo.Verify(r => r.InsertAnswer(It.Is<Answer>(a =>
                a.QuestionId == 1
                && a.Text == "Answer Text"
                && a.UserName == "farmas")));
            
        }

        [TestMethod]
        public void WhenPostingNewQuestion_ShouldRedirectWithNewQuestionInUrl()
        {
            //arrange
            var controller = GetDiaryController();
            var model = new QuestionRequestModel()
            {
                Short = "Short",
                Long = "Long"
            };

            //act
            var result = controller.AddQuestion(model) as RedirectToRouteResult;

            //assert
            Assert.AreEqual("Main", result.RouteValues["action"]);
        }

        [TestMethod]
        public void WhenPostingNewQuestion_ShouldAddQuestionToRepo()
        {
            //arrange
            var controller = GetDiaryController();
            var model = new QuestionRequestModel()
            {
                Short = "Short",
                Long = "Long"
            };

            //act
            controller.AddQuestion(model);

            //assert
            _repo.Verify(r => r.InsertQuestion(It.Is<Question>(q =>
                q.Short == "Short"
                && q.Long == "Long")));
        }

        [TestMethod]
        public void WhenGettingMain_IfSettingLatestQuestionsSize_ShouldReturnThatNumberOfLatestQuestions()
        {
            //arrange
            var controller = GetDiaryController();
            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1},
                new Question() { Short="Short2", Long="Long2", Id=2},
                new Question() { Short="Short3", Long="Long3", Id=3}
            });
            controller.LatestQuestionsMaxSize = 2;

            //act
            var result = controller.Main() as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(2, model.LatestQuestions.Count);
        }

        [TestMethod]
        public void WhenGettingMainWithQuestionId_IfQuestionIsNotInLatest_ShouldDisplayThatQuestion()
        {
            //arrange
            var controller = GetDiaryController();
            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1},
                new Question() { Short="Short2", Long="Long2", Id=2},
                new Question() { Short="Short3", Long="Long3", Id=3}
            });
            controller.LatestQuestionsMaxSize = 2;

            //act
            var result = controller.Main(1) as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(1, model.Question.Id);

            Assert.AreEqual(2, model.LatestQuestions.Count);
            Assert.AreEqual(3, model.LatestQuestions[0].Id);
            Assert.AreEqual(2, model.LatestQuestions[1].Id);
       
        }

        [TestMethod]
        public void WhenGettingMainWithQuestionId_ShouldDisplayThatQuestion()
        {
            //arrange
            var controller = GetDiaryController();
            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1},
                new Question() { Short="Short2", Long="Long2", Id=2},
                new Question() { Short="Short3", Long="Long3", Id=3}
            });

            //act
            var result = controller.Main(2) as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(2, model.Question.Id);

            Assert.AreEqual(3, model.LatestQuestions[0].Id);
            Assert.AreEqual(2, model.LatestQuestions[1].Id);
            Assert.AreEqual(1, model.LatestQuestions[2].Id);

            Assert.IsFalse(model.LatestQuestions[0].IsActive);
            Assert.IsTrue(model.LatestQuestions[1].IsActive);
            Assert.IsFalse(model.LatestQuestions[2].IsActive);
        }

        [TestMethod]
        public void WhenGettingMain_ShouldReturnAllAnswers()
        {
            //arrange
            var controller = GetDiaryController();
            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1}
            });

            _repo.Setup(r => r.GetAllAnswers()).Returns(new List<Answer>()
            {
                new Answer() { UserName = "farmas", QuestionId=1, Date = DateTime.Now },
                new Answer() { UserName = "mutter", QuestionId=1, Date = DateTime.Now.AddMinutes(1) },
                new Answer() { UserName = "exo", QuestionId=1, Date = DateTime.Now.AddMinutes(2) }
            });

            //act
            var result = controller.Main() as ViewResult;

            //assert
            Assert.AreEqual(3, ((DiaryViewModel)result.Model).Answers.Count);
        }

        [TestMethod]
        public void WhenGettingMain_ShouldMarkQuestionsAsAnsweredDependingOnStatus()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");

            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Id=1},
                new Question() { Id=2},
                new Question() { Id=3},
                new Question() { Id=4},
                new Question() { Id=5},
                new Question() { Id=6}
            });

            _repo.Setup(r => r.GetAllAnswers()).Returns(new List<Answer>()
            {
                new Answer() { UserName = "farmas", QuestionId=1 },
                new Answer() { UserName = "farmas", QuestionId=3 },
                new Answer() { UserName = "farmas", QuestionId=5 }
            });

            //act
            var result = controller.Main() as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(6, model.Question.Id);
            Assert.IsFalse(model.Question.IsAnswered);
            Assert.IsFalse(model.LatestQuestions[0].IsAnswered);
            Assert.IsTrue(model.LatestQuestions[1].IsAnswered);
            Assert.IsFalse(model.LatestQuestions[2].IsAnswered);
            Assert.IsTrue(model.LatestQuestions[3].IsAnswered);
            Assert.IsFalse(model.LatestQuestions[4].IsAnswered);
            Assert.IsTrue(model.LatestQuestions[5].IsAnswered);

        }

        [TestMethod]
        public void WhenGettingMain_IfUserHasAnswered_ShouldMarkQuestionAsAnswered()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            
            _repo.Setup(r => r.GetAllQuestions()).Returns(new List<Question>()
            {
                new Question() { Short="Short1", Long="Long1", Id=1}
            });
           
            _repo.Setup(r => r.GetAllAnswers()).Returns(new List<Answer>()
            {
                new Answer() { UserName = "farmas", QuestionId=1 }
            });

            //act
            var result = controller.Main() as ViewResult;

            //assert
            Assert.IsTrue(((DiaryViewModel)result.Model).Question.IsAnswered);

        }

        [TestMethod]
        public void WhenGettingMain_IfUserHasNotAnswered_ShouldMarkQuestionsAsNotAnswered()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");
            _repo.Setup(r => r.GetAllAnswers()).Returns(new List<Answer>());

            //act
            var result = controller.Main() as ViewResult;

            //assert
            Assert.IsFalse(((DiaryViewModel)result.Model).Question.IsAnswered);

        }

        [TestMethod]
        public void WhenGettingMain_ShouldReturnTheLoggedInUser()
        {
            //arrange
            var controller = GetDiaryController();
            _formsAuth.Setup(f => f.GetLoggedInUserName()).Returns("farmas");

            //act
            ViewResult result = controller.Main() as ViewResult;

            //assert
            Assert.AreEqual("farmas", ((DiaryViewModel) result.Model).UserName);
        }

        [TestMethod]
        public void WhenNavigateToMain_ShouldReturnTheLatestQuestion()
        {
            //arrange
            var controller = GetDiaryController();

            var questions = new List<Question>()
            {
                new Question() { Short="Short5", Long="Long5", Id=5},
                new Question() { Short="Short1", Long="Long1", Id=1}
            };
            _repo.Setup(r => r.GetAllQuestions()).Returns(questions);

            
            //act
            ViewResult result = controller.Main() as ViewResult;

            //assert
            Assert.IsInstanceOfType(result.Model, typeof(DiaryViewModel));
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(5, model.Question.Id);
            Assert.IsTrue(model.Question.IsActive);
        }

        [TestMethod]
        public void WhenGettingMain_IfLessThan10Questions_ShouldReturnAllLatestQuestions()
        {
            //arrange
            var controller = GetDiaryController();
            var questions = new List<Question>()
            {
                new Question() { Short="Short5", Long="Long5", Id=5},
                new Question() { Short="Short2", Long="Long2", Id=2},
                new Question() { Short="Short4", Long="Long4", Id=4},
                new Question() { Short="Short3", Long="Long3", Id=3},
                new Question() { Short="Short1", Long="Long1", Id=1}
            };
            _repo.Setup(r => r.GetAllQuestions()).Returns(questions);


            //act
            ViewResult result = controller.Main() as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(5, model.Question.Id);

            Assert.AreEqual(5, model.LatestQuestions.Count);
            Assert.AreEqual(5, model.LatestQuestions[0].Id);
            Assert.AreEqual(4, model.LatestQuestions[1].Id);
            Assert.AreEqual(3, model.LatestQuestions[2].Id);
            Assert.AreEqual(2, model.LatestQuestions[3].Id);
            Assert.AreEqual(1, model.LatestQuestions[4].Id);
        }

        [TestMethod]
        public void WhenGettingMain_IfMoreThan10Questions_ShouldReturn10LatestQuestions()
        {
            //arrange
            var controller = GetDiaryController();
            var questions = new List<Question>()
            {
                new Question() { Short="Short", Long="Long", Id=1},
                new Question() { Short="Short", Long="Long", Id=2},
                new Question() { Short="Short", Long="Long", Id=3},
                new Question() { Short="Short", Long="Long", Id=4},
                new Question() { Short="Short", Long="Long", Id=5},
                new Question() { Short="Short", Long="Long", Id=6},
                new Question() { Short="Short", Long="Long", Id=7},
                new Question() { Short="Short", Long="Long", Id=8},
                new Question() { Short="Short", Long="Long", Id=9},
                new Question() { Short="Short", Long="Long", Id=10},
                new Question() { Short="Short", Long="Long", Id=11},
                new Question() { Short="Short", Long="Long", Id=12},
            };
            _repo.Setup(r => r.GetAllQuestions()).Returns(questions);


            //act
            ViewResult result = controller.Main() as ViewResult;

            //assert
            var model = (DiaryViewModel)result.Model;
            Assert.AreEqual(12, model.Question.Id);

            Assert.AreEqual(10, model.LatestQuestions.Count);
            Assert.AreEqual(12, model.LatestQuestions[0].Id);
            Assert.AreEqual(11, model.LatestQuestions[1].Id);
            Assert.AreEqual(10, model.LatestQuestions[2].Id);
            Assert.AreEqual(9, model.LatestQuestions[3].Id);
            Assert.AreEqual(8, model.LatestQuestions[4].Id);
            Assert.AreEqual(7, model.LatestQuestions[5].Id);
            Assert.AreEqual(6, model.LatestQuestions[6].Id);
            Assert.AreEqual(5, model.LatestQuestions[7].Id);
            Assert.AreEqual(4, model.LatestQuestions[8].Id);
            Assert.AreEqual(3, model.LatestQuestions[9].Id);
        }

        
    }
}
