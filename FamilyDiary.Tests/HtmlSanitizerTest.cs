﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FamilyDiary.Tests
{
    [TestClass]
    public class HtmlSanitizerTest
    {
        [TestMethod]
        public void WhenNoTags_ShouldReturnSameString()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual("foobar", s.Sanitize("foobar"));
        }


        [TestMethod]
        public void WhenUnknownTag_ShouldEncode()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual("&lt;foo&gt;", s.Sanitize("<foo>"));
        }


        [TestMethod]
        public void WhenKnownTags_IfUpperCase_ShouldNotEncode()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual(
                "&lt;FOO&gt;<B></B><I></I><U></U><S></S><UL></UL><OL></OL><LI></LI><DIV></DIV><A></A><IMG></IMG>",
                s.Sanitize("<FOO><B></B><I></I><U></U><S></S><UL></UL><OL></OL><LI></LI><DIV></DIV><A></A><IMG></IMG>"));
        }

        [TestMethod]
        public void WhenKnownTags_ShouldNotEncode()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual(
                "&lt;foo&gt;<br><b></b><i></i><u></u><s></s><ul></ul><ol></ol><li></li><div></div><a></a><img></img>",
                s.Sanitize("<foo><br><b></b><i></i><u></u><s></s><ul></ul><ol></ol><li></li><div></div><a></a><img></img>"));
        }

        [TestMethod]
        public void WhenImageOrAnchorWithAttributes_ShouldNotEncode()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual(
                "<a href=\"foo\"><img src=\"foo\">",
                s.Sanitize("<a href=\"foo\"><img src=\"foo\">"));
        }

        [TestMethod]
        public void WhenUnknwonTagInsideKnown_ShouldEncodeUnknown()
        {
            var s = new HtmlSanitizer();
            Assert.AreEqual(
                "<b>&lt;f&gt;</b>",
                s.Sanitize("<b><f></b>"));
        }
    }
}
