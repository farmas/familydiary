﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PostmarkDotNet;
using System.Configuration;

namespace FamilyDiary
{
    public class PostmarkEmailService: IEmailService
    {
        private static string postmarkToken = ConfigurationManager.AppSettings["POSTMARK_TOKEN"];

        public void Send(System.Net.Mail.MailMessage message)
        {
            var postMarkMessage = new PostmarkMessage(message);

            var client = new PostmarkClient(postmarkToken);
            client.SendMessage(postMarkMessage);
        }
    }
}