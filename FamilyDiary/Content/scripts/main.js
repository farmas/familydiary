//namespace
var farmas = {};

(function () {
    farmas.setupClickableDivs = function(){
        $('.clickable:not(.active-question)').click(function () {
            window.location = $(this).find('input').val();
        });
    };    

    farmas.setupNewQuestionDialog = function() {
        //prepare the dialog
        var newQuestionDialog = $('#newQuestionDialog');
        var newQuestionForm = $('#newQuestionForm');
        

        newQuestionDialog.dialog({
            autoOpen: false,
            modal:true,
            width:700,
            buttons:{
                "Crear": function(){
                    var $createButton = $('.ui-button-text')
                                .filter(function(){ return $(this).text() === 'Crear' })
                                .parent();

                    if(newQuestionForm.valid()){
                        $createButton.attr('disabled','disabled');
                        newQuestionForm.submit();
                    }
                },
                "Cancelar": function(){
                    $(this).dialog("close");
                }
            }
        });

        // hookup the button to show the dialog
        $('#newQuestionDiv').click(function(){
            newQuestionDialog.dialog("open");
        });

        // prepare the form validation
         newQuestionForm.validate({
            messages: {
                short: " Epale!",
                long: " Epale!"
            }
        });
    };

    farmas.setupAnswerButton = function() {
        $('#answerButton').button().click(function () {
            $('#answerButton').attr('disabled','disabled');
            $('#newAnswerDiv').hide();

            var parentDiv = $(this).parent();
            var url = parentDiv.find('#url').val();
            var answerText = $('#answerText').wysiwyg('getContent');
            var data =
            {
                questionid: parentDiv.find('#questionid').val(),
                username: parentDiv.find('#username').val(),
                text: answerText,
                sendMail: $('#answerSendMailCheckbox').is(':checked')
            };

            $('#message').show();

            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: function (data) {
                    var answerDiv = $('#newAnswerDiv');
                    answerDiv.find('.answer-text').html(data);

                    $('#answerDiv').slideUp(500, function () {
                        answerDiv.slideDown(500);
                    });
                    $('.active-question').find('img').hide();
                },
                error: function () {
                    $("#message")
                        .css("background-color", "red")
                        .html("Hubo un error (T_T). Intenta otra vez.");
                }
            });
        });
    };

    farmas.setupOldQuestionAnimations = function() {
        $('.old-question:not(.active-question)').hover(function () {
            $(this).animate(
                { marginLeft: '0px' },
                200);
        }, function () {
            $(this).animate(
                { marginLeft: '25px' },
                200);
        });
    };

    farmas.setupRichTextBox = function() {
        $('#answerText').wysiwyg({
            initialContent: '',
            autoGrow: true,
            controls: {
                bold: { visible: true },
                italic: { visible: true },
                h1: { visible: false },
                h2: { visible: false },
                h3: { visible: false },
                indent: { visible: false },
                insertHorizontalRule: { visible: false },
                insertTable: { visible: false },
                justifyCenter: { visible: false },
                justifyRight: { visible: false },
                justifyFull: { visible: false },
                justifyLeft: { visible: false },
                outdent: { visible: false },
                undo: { visible: false },
                redo: { visible: false },
                removeFormat: { visible: false },
                subscript: { visible: false },
                superscript: { visible: false }
            }
        });
    }
} ());

$(document).ready(function () {
    farmas.setupClickableDivs();
    farmas.setupAnswerButton();
    farmas.setupOldQuestionAnimations();
    farmas.setupNewQuestionDialog();
    farmas.setupRichTextBox();
});










