$(document).ready(function () {
    $('.submitButton').button();

    var users = $('.user');
    users.fadeTo(0, 0.5);
    users.find('input').hide();

    users.click(function () {
        $(this).find('div')
			.animate(
				{ paddingTop: '-=15' },
				200)
			.find('input').show();
        $(this).find('.user-text').focus();
    });

    users.hover(function () {
        $(this).fadeTo(200, 1)
				.animate(
					{ height: '+=20' },
					{ duration: 200, queue: false })
				.find('img').animate(
					{ width: '+=10', height: '+=10' },
					200);
    },
		function () {
		    $(this).fadeTo(200, 0.5)
				.animate(
					{ height: '50' },
					{ duration: 200, queue: false })
				.find('div').animate(
					{ paddingTop: '15' },
					200)
				.find('input').hide();
		    $(this).find('img').animate(
				{ width: '50px', height: '50px' },
				200);
		});
});