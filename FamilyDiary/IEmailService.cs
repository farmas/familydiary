﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace FamilyDiary
{
    public interface IEmailService
    {
        void Send(MailMessage message);
    }
}
