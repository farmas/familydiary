﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamilyDiary.Models;
using System.Web.Routing;
using Microsoft.Web.Mvc;
using System.Text.RegularExpressions;
using FamilyDiary.Models.Account;
using Ninject;
using System.Net.Mail;
using System.Text;
using FamilyDiary.RequestModels;

namespace FamilyDiary.Controllers
{
    public class DiaryController : Controller
    {
        [Inject]
        public IRepository Repository { get; set; }

        [Inject]
        public IFormsAuthenticationService FormsService { get; set; }
        
        [Inject]
        public IHtmlSanitizer HtmlSanitizer { get; set; }

        [Inject]
        public IEmailService EmailService { get; set; }

        [Inject]
        public IMembershipService MembershipService { get; set; }

        [Inject]
        public IFileSystem FileSystem { get; set; }

        public int LatestQuestionsMaxSize { get; set; }

        public DiaryController()
        {
            this.LatestQuestionsMaxSize = 10;
        }

        [Authorize]
        [HttpGet]
        public ActionResult Main(int? questionId = null)
        {

            var model = new DiaryViewModel();
            model.UserName = FormsService.GetLoggedInUserName();

            // prepare the latest questions list
            var userAnswers = from a in Repository.GetAllAnswers()
                              where a.UserName == model.UserName
                              select new { QuestionId = a.QuestionId };


            var latestQuestions = (from q in Repository.GetAllQuestions()
                                   orderby q.Id descending
                                   select new QuestionViewModel(q)
                                   {
                                       IsAnswered = userAnswers.Any(a => a.QuestionId == q.Id)
                                   }).Take(LatestQuestionsMaxSize);

            model.LatestQuestions = latestQuestions.ToList();

            // prepare the active question
            if (!questionId.HasValue)
            {
                model.Question = model.LatestQuestions.First();
            }
            else
            {
                model.Question = model.LatestQuestions.FirstOrDefault(q => q.Id == questionId.Value);

                if (model.Question == null)
                {
                    var question = Repository.GetAllQuestions().First(q => q.Id == questionId.Value);
                    model.Question = new QuestionViewModel(question)
                    {
                        IsAnswered = userAnswers.Any(a => a.QuestionId == question.Id)
                    };
                }
            }

            model.Question.IsActive = true;
            model.Answers = (from a in Repository.GetAllAnswers()
                          where a.QuestionId == model.Question.Id
                          orderby a.Id descending
                          select a).ToList();

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddQuestion([Bind(Prefix="")] QuestionRequestModel model)
        {
            model.Id = 0;
            var question = new Question();
            ModelCopier.CopyModel(model, question);
            question.Date = DateTime.Now;
        
            int questionId = Repository.InsertQuestion(question);

            if (model.SendMail)
            {
                var username = FormsService.GetLoggedInUserName();

                var emailBody = FileSystem.FileReadAllText(this.Server.MapPath("~/Views/Emails/NewQuestionEmail.htm"));
                emailBody = emailBody.Replace("{QUESTION}", model.Long)
                    .Replace("{USERNAME}", username)
                    .Replace("{QUESTIONID}", questionId.ToString());

                SendEmail(
                    subject: String.Format("Nueva pregunta de {0}!", username),
                    body: emailBody);
            }
            return RedirectToAction("Main");
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddAnswer(AnswerRequestModel model)
        {
            model.Text = HtmlSanitizer.Sanitize(model.Text);
            model.Id = 0;

            var answer =  new Answer();
            ModelCopier.CopyModel(model, answer);
            answer.Date = DateTime.Now;
            answer.UserName = FormsService.GetLoggedInUserName();

            Repository.InsertAnswer(answer);

            if (model.SendMail)
            {
                string question = Repository.GetAllQuestions().Where(q => q.Id == model.QuestionId).FirstOrDefault().Long;

                var emailBody = FileSystem.FileReadAllText(this.Server.MapPath("~/Views/Emails/NewAnswerEmail.htm"));
                emailBody = emailBody.Replace("{QUESTION}", question)
                    .Replace("{USERNAME}", answer.UserName)
                    .Replace("{ANSWER}", answer.Text)
                    .Replace("{QUESTIONID}", answer.QuestionId.ToString());

                SendEmail(
                    subject: String.Format("Nueva respuesta de {0}!", answer.UserName),
                    body: emailBody);
            }
            return Json(model.Text);
        }

        private void SendEmail(string subject, string body)
        {
            var message = new MailMessage(
                from: "fede@farmas.net",
                to: String.Join(",", MembershipService.GetAllUserEmailAddress()),
                subject: subject,
                body: body);

            message.IsBodyHtml = true;
            message.ReplyToList.Add(new MailAddress("fsilva.armas@gmail.com"));

            EmailService.Send(message);
        }
    }
}
