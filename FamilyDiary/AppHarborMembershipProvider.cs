﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Reflection;

namespace FamilyDiary
{
    public class AppHarborMembershipProvider : SqlMembershipProvider
    {
        private static string connectionString = ConfigurationManager.AppSettings["MEMBERSHIP_CONNECTION_STRING"];

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);

            FieldInfo connectionStringField = GetType().BaseType.GetField("_sqlConnectionString", BindingFlags.Instance | BindingFlags.NonPublic);
            connectionStringField.SetValue(this, connectionString);
        }
    }
}