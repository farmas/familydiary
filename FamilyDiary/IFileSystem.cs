﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FamilyDiary
{
    public interface IFileSystem
    {
        string FileReadAllText(string path);
    }
}
