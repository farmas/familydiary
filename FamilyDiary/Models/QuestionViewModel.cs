﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Web.Mvc;

namespace FamilyDiary.Models
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Short { get; set; }
        public string Long { get; set; }
        public DateTime Date { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsActive { get; set; }

        public QuestionViewModel(Question question)
        {
            ModelCopier.CopyModel(question, this);
        }

        public QuestionViewModel()
        {
        }
    }
}