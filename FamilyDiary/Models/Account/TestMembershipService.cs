﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.Models.Account
{
    public class TestMembershipService: IMembershipService
    {
        public int MinPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public bool ValidateUser(string userName, string password)
        {
            return true;
        }

        public System.Web.Security.MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            throw new NotImplementedException();
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }


        public string[] GetAllUserNames()
        {
            var users = new List<string>()
            {
                "Fedus", "Clots", "Exo", "Mutter", "Sallie", "Ricardo"
            };
            return users.ToArray();
        }


        public string[] GetAllUserEmailAddress()
        {
            return new string[1] { "fsilva.armas@gmail.com" };
        }
    }
}