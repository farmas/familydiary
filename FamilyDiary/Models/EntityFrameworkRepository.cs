﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.Models
{
    public class EntityFrameworkRepository: IRepository
    {
        public IList<Question> GetAllQuestions()
        {
            var entities = new Entities();
            return entities.Questions.ToList();
        }

        public IList<Answer> GetAllAnswers()
        {
            var entities = new Entities();
            return entities.Answers.ToList();
        }

        public int InsertQuestion(Question question)
        {
            var entities = new Entities();
            entities.Questions.AddObject(question);
            entities.SaveChanges();
            return question.Id;
        }

        public int InsertAnswer(Answer answer)
        {
            var entities = new Entities();
            entities.Answers.AddObject(answer);
            entities.SaveChanges();
            return answer.Id;
        }
    }
}