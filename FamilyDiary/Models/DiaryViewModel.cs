﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.Models
{
    public class DiaryViewModel
    {
        public QuestionViewModel Question { get; set; }
        public IList<QuestionViewModel> LatestQuestions { get; set; }

        public string UserName { get; set; }

        public IList<Answer> Answers { get; set; }
    }
}