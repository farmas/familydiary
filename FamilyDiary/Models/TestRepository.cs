﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.Models
{
    public class TestRepository: IRepository
    {
        private List<Question> _questions;
        private List<Answer> _answers;


        public TestRepository()
        {
            if (HttpContext.Current.Session["q"] == null)
            {
                HttpContext.Current.Session["q"] = new List<Question>()
                {
                    new Question() 
                    { 
                        Id=1, 
                        Short="Super poderes", 
                        Long="¿Si pudieras tener un super poder, cual sería?",
                        Date = DateTime.Now
                    },
                    new Question() 
                    { 
                        Id=2, 
                        Short="Libros", 
                        Long="¿Cuál era tu libro favorito de jóven?",
                        Date = DateTime.Now
                    },
                    new Question() 
                    { 
                        Id=3, 
                        Short="Miedos", 
                        Long="¿Cual era uno de tus miedos cuando eras niño?",
                        Date = DateTime.Now
                    },
                    new Question() 
                    { 
                        Id=4, 
                        Short="Clases", 
                        Long="¿Cuál era tu clase favorita en primaria o secundaria?",
                        Date = DateTime.Now
                    }
                };
                HttpContext.Current.Session["a"] = new List<Answer>();

            }

            _questions = (List<Question>)HttpContext.Current.Session["q"];
            _answers = (List<Answer>)HttpContext.Current.Session["a"];
        }


        public IList<Question> GetAllQuestions()
        {
            return _questions;
        }


        public IList<Answer> GetAllAnswers()
        {
            return _answers;
        }


        public int InsertQuestion(Question question)
        {
            _questions.Add(question);
            question.Id = _questions.Count;
            HttpContext.Current.Session["q"] = _questions;
            return question.Id;
        }


        public int InsertAnswer(Answer answer)
        {
            _answers.Add(answer);
            answer.Id = _answers.Count;
            HttpContext.Current.Session["a"] = _answers;
            return answer.Id;
        }
    }
}