﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.Models
{
    public interface IRepository
    {
        IList<Question> GetAllQuestions();
        IList<Answer> GetAllAnswers();

        int InsertQuestion(Question question);
        int InsertAnswer(Answer answer);
    }
}