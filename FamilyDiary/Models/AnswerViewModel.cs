﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamilyDiary.Models
{
    public class AnswerViewModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string UserName { get; set; }

        [AllowHtml]
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}