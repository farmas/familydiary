﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace FamilyDiary
{
    public class FileSystem: IFileSystem
    {
        public string FileReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }
}