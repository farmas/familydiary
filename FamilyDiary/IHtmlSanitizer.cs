﻿using System;
namespace FamilyDiary
{
    public interface IHtmlSanitizer
    {
        string Sanitize(string html);
    }
}
