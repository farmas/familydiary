﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FamilyDiary.RequestModels
{
    public class AnswerRequestModel
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string UserName { get; set; }
        public bool SendMail { get; set; }

        [AllowHtml]
        public string Text { get; set; }
    }
}