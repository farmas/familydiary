﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FamilyDiary.RequestModels
{
    public class QuestionRequestModel
    {
        public int Id { get; set; }
        public string Short { get; set; }
        public string Long { get; set; }
        public bool SendMail { get; set; }
    }
}