﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Security.Application;

namespace FamilyDiary
{
    public class AntiXssHtmlSanitizer: IHtmlSanitizer
    {
        public string Sanitize(string html)
        {
            return Sanitizer.GetSafeHtmlFragment(html);
        }
    }
}