﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FamilyDiary.Models;
using FamilyDiary.Models.Account;
using System.Configuration;
using System.Web.Configuration;

namespace FamilyDiary
{
    public class DiaryNinjectModule: Ninject.Modules.NinjectModule
    {
        private static string mode = ConfigurationManager.AppSettings["MODE"];

        public override void Load()
        {
            Bind<IFormsAuthenticationService>().To<FormsAuthenticationService>();
            Bind<IHtmlSanitizer>().To<AntiXssHtmlSanitizer>();
            Bind<IFileSystem>().To<FileSystem>();

            if (mode.Equals("Demo"))
            {
                Bind<IRepository>().To<TestRepository>();
                Bind<IEmailService>().To<TestEmailService>();
                Bind<IMembershipService>().To<TestMembershipService>();
            }
            else
            {
                Bind<IRepository>().To<EntityFrameworkRepository>();
                Bind<IEmailService>().To<PostmarkEmailService>();
                Bind<IMembershipService>().To<MembershipService>();
            }
        }
    }
}