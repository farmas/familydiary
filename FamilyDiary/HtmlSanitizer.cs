﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace FamilyDiary
{
    public class HtmlSanitizer : IHtmlSanitizer
    {
        public string Sanitize(string html)
        {
            var encoded = HttpUtility.HtmlEncode(html);

            // TODO: this sanitizer is over-agressive, if an element's attribute has &gt; it will
            // only decode up to that character. However, there should not be XSS vulnerabilities.
            // To be replaced once I find a proper anti xss library that works in medium trust.
            encoded = Regex.Replace(
                encoded,
                @"&lt;/?(b|i|u|ul|ol|li|s|div|a|img|br).*?&gt;",
                r => HttpUtility.HtmlDecode(r.Value), 
                RegexOptions.IgnoreCase);
            
            return encoded;
        }

    }
}